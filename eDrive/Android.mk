LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := eDrive
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true


ifeq ($(PLATFORM_SDK_VERSION), 25)
  LOCAL_SRC_FILES := foundation.e.drive.alpha-1-n-build-19-06-28T1318-release-unsigned.apk
else
  LOCAL_SRC_FILES := foundation.e.drive.alpha-1-o-build-19-07-01T1701-release-unsigned.apk
endif

LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
include $(BUILD_PREBUILT)
